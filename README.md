# Automatic generation of blending words: distributional approach to semantic classification

This GitLab project is dedicated to my second Master's thesis, about the automatic generation of blending words.
More information about the thesis per say is provided in the abstract (in french and english) of the `submitted_thesis.pdf` file, or in the `one_page_summary.pdf` file (english only).

The present file is here to describe the GitLab project's structure.

## Folders

- `data`: CSV files containing attested blends (annotated)

- `lexicons`: CSV files containing the words for the semantic classes extracted from the FrSemCor corpus

- `models`: Word2Vec models used in the thesis as KeyedVectors, whether it'd be [Fauconnier](https://fauconnier.github.io/)'s originals, or the ones we created by reduction using PCA

- `notebooks`: jupyter notebooks used for the thesis

## The `data.csv` file

The file is structured as a table, each row representing an entry, and each column being a field.
Here's a quick description of the different fields:

- `blend`, `base_1`, `base_2` are for orthograhic forms of the entries

- `frsemcor_X` are for the reference semantic annotation (manual). `X`=0,1,2 for the blend, base\_1 and base\_2 respectively

- `predictions` is for the generated semantic class

There are other fields, which were useful for earlier attempts to tackle the subject of the thesis.
These include most notably:

- `sampa_X`: the SAMPA phonetic transcription for a given form, obtained manually

- `synset_X`: the synset of the given form in NLTK's French WordNet (using the Open Multilingual WordNet code `'fra'`, Sagot & Darja's [WOLF](http://pauillac.inria.fr/~sagot/index.html#wolf)), obtained manually by considering the meaning of the form used to create the blend's meaning

- `UB_X`: the Unique Beginner of the given form in NLTK's French WordNet

- `cat_gram_X`: the grammatical category of the given form

## Using this project

I might provide a genuine Python module in the future, and documenting it using reStructured Text convention.
Feel free to download and  copy the code in the notebooks.
